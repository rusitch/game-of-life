﻿namespace GameOfLife
{
	public class Life
	{
		private IndividualState[,] Map { get; set; }
		private int Width { get; }
		private int Heigth { get; }

		public Life(int width, int height)
		{
			Width = width;
			Heigth = height;

			InitializeMap();
		}

		public IndividualState TogglePoint(int x, int y)
		{
			Map[x, y] = Map[x, y] == IndividualState.Empty
				? IndividualState.Alive
				: IndividualState.Empty;

			return Map[x, y];
		}

		public IndividualState GetPoint(int x, int y)
		{
			if (x < 0 || x >= Width || y < 0 || y >= Heigth)
				return IndividualState.Empty;

			return Map[x, y];
		}

		public void PrepareNextStep()
		{
			for (var i = 0; i < Width; i++)
			{
				for (var j = 0; j < Heigth; j++)
				{
					var neighborsNumber = CalculateNeighbors(i, j);


					if (Map[i, j] == IndividualState.Alive && (neighborsNumber <= 1 || neighborsNumber >= 4))
					{
						Map[i, j] = IndividualState.WillDie;
					}

					if (Map[i, j] == IndividualState.Empty && neighborsNumber == 3)
					{
						Map[i, j] = IndividualState.WillBorn;
					}
				}
			}
		}

		public void NextStep()
		{
			for (var i = 0; i < Width; i++)
			{
				for (var j = 0; j < Heigth; j++)
				{
					switch (Map[i, j])
					{
						case IndividualState.WillBorn:
							Map[i, j] = IndividualState.Alive;
							break;
						case IndividualState.WillDie:
							Map[i, j] = IndividualState.Empty;
							break;
					}
				}
			}
		}

		private void InitializeMap()
		{
			Map = new IndividualState[Width, Heigth];
			for (var i = 0; i < Width; i++)
			{
				for (var j = 0; j < Heigth; j++)
				{
					Map[i, j] = IndividualState.Empty;
				}
			}
		}

		private int CalculateNeighbors(int x, int y)
		{
			var number = 0;
			for (var i = -1; i <= 1; i++)
			{
				for (var j = -1; j <= 1; j++)
				{
					if (i == 0 && j == 0)
						continue;

					var result = GetPoint(x + i, y + j);
					if (result == IndividualState.Alive || result == IndividualState.WillDie)
					{
						number++;
					}
				}
			}

			return number;
		}
	}
}
