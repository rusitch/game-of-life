﻿namespace GameOfLife
{
	public enum IndividualState
	{
		WillBorn = -1,
		Empty = 0,
		Alive = 1,
		WillDie = 2
	}
}
