﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GameOfLife
{
	public partial class MainForm : Form
	{
		private const int BoxWidth = 20;
		private const int BoxHeight = 20;

		private int _fieldHeight;
		private int _fieldWidth;

		private Label[,] _boxes;
		private Life _life;

		public MainForm()
		{
			InitializeComponent();
			InitLabels();
		}

		private void InitLabels()
		{
			_fieldWidth = (lifeField.Width - 1) / BoxWidth;
			_fieldHeight = (lifeField.Height - 1) / BoxHeight;

			_life = new Life(_fieldWidth, _fieldHeight);
			_boxes = new Label[_fieldWidth, _fieldHeight];

			for (var i = 0; i < _fieldWidth; i++)
				for (var j = 0; j < _fieldHeight; j++)
					AddBox(i, j);
		}

		private void AddBox(int x, int y)
		{
			var label = new Label
			{
				BorderStyle = BorderStyle.FixedSingle,
				Location = new Point(x * BoxWidth, y * BoxHeight),
				Size = new Size(BoxWidth + 1, BoxHeight + 1),
				Parent = lifeField
			};
			label.MouseClick += box_MouseClick;

			_boxes[x, y] = label;
		}

		private void box_MouseClick(object sender, MouseEventArgs e)
		{
			var x = ((Label)sender).Location.X / BoxWidth;
			var y = ((Label)sender).Location.Y / BoxHeight;
			var boxState = _life.TogglePoint(x, y);

			_boxes[x, y].BackColor = GetColor(boxState);
		}

		private void RefreshField()
		{
			for (var i = 0; i < _fieldWidth; i++)
				for (var j = 0; j < _fieldHeight; j++)
					_boxes[i, j].BackColor = GetColor(_life.GetPoint(i, j));
		}

		private Color GetColor(IndividualState state)
		{
			switch (state)
			{
				case IndividualState.WillBorn:
					return Color.Yellow;
				case IndividualState.Empty:
					return Color.White;
				case IndividualState.Alive:
					return Color.Black;
				case IndividualState.WillDie:
					return Color.Red;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void cycleTimer_Tick(object sender, EventArgs e)
		{
			_life.PrepareNextStep();
			_life.NextStep();
			RefreshField();
		}

		private void runButton_Click(object sender, EventArgs e)
		{
			cycleTimer.Enabled = !cycleTimer.Enabled;
		}
	}
}
